package com.overxak.miapi.core;

import com.overxak.miapi.core.eis.bo.Carrera;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OverxakApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OverxakApiApplication.class, args);

		
	}
	
}
