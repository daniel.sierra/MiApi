package com.overxak.miapi.core.eis.bo;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "profesor")
public class Profesor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="codigo_profesor")
    private Integer codigoProfesor;

    @Column
    private String nombreProfesor;

    @Column
    private String direccion;

    @Column
    private String telefono;

    @Column
    private String comentario;

    @Column
    private String statusPersona;

    @Column
    private String areaProfesor;



}
