package com.overxak.miapi.core.eis.bo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;


@Entity
@Data
@Table(name="asignacion_alumno_carrera")
public class AsignacionAlumnoCarrera implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="codigo_asignacion_alumno_carrera")
    private Integer codigoAsignacionAlumno;

    @Column
    private Date fecha_asignacion_carrera;

    @Column
    private String comentarios;

    @Column
    private Integer status;

    @ManyToOne
    @JoinColumn(name="codigo_carrera", nullable=false,insertable=false,updatable=false)
    private Carrera carrera;

    @ManyToOne
    @JoinColumn(name = "carne", nullable = false,insertable = false, updatable = false )
    private Alumno alumno;

}
