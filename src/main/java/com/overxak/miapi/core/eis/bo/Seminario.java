package com.overxak.miapi.core.eis.bo;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;
import java.util.Date;

@Entity
@Data
@Table(name = "seminario")
public class Seminario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="codigo_seminario")
    private Integer codigoSeminario;

    @Column
    private String nombreSeminario;

    @Column
    private Date fechaInicio;

    @Column
    private Date fechaFin;

    @ManyToOne
    @JoinColumn(name="codigo_carrera", nullable=false,insertable=false,updatable=false)
    private Carrera carrera;

    @ManyToOne
    @JoinColumn(name="codigo_profesor", nullable=false,insertable=false,updatable=false)
    private Profesor profesor;


}

