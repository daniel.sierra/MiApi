package com.overxak.miapi.core.eis.bo;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="jornada")
public class Jornada implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="codigo_jornada")
    private Integer codigoJornada;

    @Column(name="nombre_jornada")
    private String nombreJornada;

    @Column(name="status")
    private String status;

}
