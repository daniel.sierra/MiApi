package com.overxak.miapi.core.eis.bo;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="municipio")
public class Municipio implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="codigo_municipio")
    private Integer codigoMunicipio;

    @Column(name="nombre")
    private String nombre;

    @Column(name="codigo_departamento")
    private String codigoDepartamento;

    @ManyToOne
    @JoinColumn(name="codigo_departamento", nullable=false,insertable=false,updatable=false)
    private Departamento departamento;

    @OneToMany(mappedBy="municipio")
    private Set<Municipio> municipio;

}
