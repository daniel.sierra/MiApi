package com.overxak.miapi.core.eis.bo;

import java.io.Serializable;
import javax.persistence.*
        ;
@Embeddable
public class AlumnoAsignacionId implements Serializable {

    @Column(name="carne",nullable=false,insertable=false,updatable=false)
    private String carne;

    @Column(name="codigo_jornada",nullable=false,insertable=false,updatable=false)
    private Integer codigoJornada;

}
