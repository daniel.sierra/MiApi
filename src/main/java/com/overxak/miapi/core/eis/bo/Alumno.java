package com.overxak.miapi.core.eis.bo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="alumno")
public class Alumno implements Serializable {

	@Id
	@Column(name="carne")
	private String carne;

	@Column(name="tipo_aporte")
	private String tipoAporte;

	@Column(name="status_persona")
	private String statusPersona;

	@Column(name="nombres")
	private String nombres;

	@Column(name="apellidos")
	private String apellidos;

	@Column(name="direccion")
	private String direccion;

	@Column(name="docto_identidad")
	private Integer doctoIdentidad;

	@Column(name="telefono1")
	private Integer telefono1;

	@Column(name="telefono2")
	private Integer telefono2;

	@Column(name="email")
	private String email;

	@Column(name="comentario")
	private String comentario;

	@Column(name="fecha_ingreso")
	private Date fechaIngreso;

	@Column(name="fecha_nacimiento")
	private Date fechaNacimiento;

	@Column(name="forma_conocimiento_kinal")
	private String formaConocimientoKinal;

	@Column(name="ultimo_grado_aprobado")
	private String ultimoGradoAprobado;

	@Column(name="empresa_labora")
	private String empresaLabora;

	@Column(name="pago_empresa")
	private Integer pagoEmpresa;

	@Column(name="puesto_empresa")
	private String puestoEmpresa;

	@Column(name="direccion_trabajo")
	private String direccionTrabajo;

	@Column(name="telefono_trabajo")
	private Integer telefonoTrabajo;

	@Column(name="nombre_recibo")
	private String nombreRecibo;

	@Column(name="nit")
	private Integer nit;

	@Column(name="valor_aporte")
	private Integer valorAporte;

	@Column(name="nombre_completo")
	private String nombreCompĺeto;

	@Column(name="codigo_municipio")
	private Integer codigoMunicipio;

	@Column(name="codigo_departamento")
	private Integer codigoDepartamento;

	@ManyToOne
	@JoinColumn(name="codigo_departamento", nullable=false,insertable=false,updatable=false)
	private Departamento departamento;

	@ManyToOne
	@JoinColumn(name="codigo_municipio", nullable=false,insertable=false,updatable=false)
	private Municipio municipio;
}
