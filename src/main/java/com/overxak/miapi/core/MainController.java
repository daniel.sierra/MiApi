package com.overxak.miapi.core;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import com.overxak.miapi.core.eis.bo.Carrera;

@Controller
public class MainController {
    @Autowired
    @RequestMapping("/")
    @ResponseBody
    public String Index() {
        String response = "Iniciando el Proyecto";

        return response;
    }
}